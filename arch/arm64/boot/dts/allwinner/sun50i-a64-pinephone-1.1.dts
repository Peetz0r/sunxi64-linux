// SPDX-License-Identifier: (GPL-2.0+ OR MIT)
// Copyright (C) 2020 Ondrej Jirman <megous@megous.com>

/dts-v1/;

#include "sun50i-a64-pinephone.dtsi"

/ {
	model = "Pine64 PinePhone Braveheart (1.1)";
	compatible = "pine64,pinephone-1.1", "allwinner,sun50i-a64";
};

&anx7688 {
	reset-gpios = <&r_pio 0 9 GPIO_ACTIVE_HIGH>; /* PL9 */
	avdd33-supply = <&reg_dldo1>;
};

&axp803 {
	x-powers,drive-vbus-en;
};

&backlight {
	power-supply = <&reg_ldo_io0>;
	/*
	 * PWM backlight circuit on this PinePhone revision was changed since
	 * 1.0, and the lowest PWM duty cycle that doesn't lead to backlight
	 * being off is around 20%. Duty cycle for the lowest brightness level
	 * also varries quite a bit between individual boards, so the lowest
	 * value here was chosen as a safe default.
	 */
	brightness-levels = <
		392  413  436  468
		512  571  647  742
		857  995  1159 1349
		1568 1819 2103 2423
		2779 3176 3614 4096>;
	num-interpolated-steps = <50>;
	default-brightness-level = <400>;
};

&codec_analog {
	allwinner,internal-bias-resistor;
};

/*
 * The N_VBUSEN pin is disconnected, but we need to inform the PMIC about
 * the VBUS status anyway. To avoid the pin from floating and to inform
 * the PMIC, about VBUS status, we couple reg_drivevbus with reg_vbus.
 */
&reg_drivevbus {
	vin-supply = <&reg_vcc5v0>;
	status = "okay";
};

&reg_usb0_vbus {
	gpio = <&pio 3 6 GPIO_ACTIVE_HIGH>; /* PD6 */
	enable-active-high;
	vin-supply = <&reg_drivevbus>;
};

&ring_indicator {
	gpios = <&pio 1 2 GPIO_ACTIVE_LOW>; /* PB2 */
};

&sgm3140 {
	enable-gpios = <&pio 3 24 GPIO_ACTIVE_HIGH>; /* PD24 */
	flash-gpios = <&pio 2 3 GPIO_ACTIVE_HIGH>; /* PC3 */
};
